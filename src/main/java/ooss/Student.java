package ooss;



public class Student extends Person implements MyObserver {

    private Klass klass;
    public Student(int id, String name, int age) {
        super(id, name, age);
    }

    public void join(Klass klass) {
        this.klass = klass;
    }

    public Boolean isIn(Klass klass) {
        return klass.equals(this.klass);
    }

    @Override
    public String introduce() {
        String result = "My name is "+ getName() +". I am "+getAge()+" years old. I am a student.";
        if (this.klass != null) {
            result += klass.isLeader(this)? " I am the leader of class "+ klass.getNumber() + "." :" I am in class "+ klass.getNumber() + ".";
        }
        return result;
    }

    public Klass getKlass() {
        return klass;
    }

    @Override
    public void response(int number,String leaderName) {
        System.out.println("I am "+getName()+", student of Class "+number+". I know "+leaderName+" become Leader.");
    }
}
