package ooss;

import org.graalvm.compiler.core.common.type.ArithmeticOpTable;

import java.util.ArrayList;
import java.util.Objects;

public class Klass extends Notice {

    private int number;
    private Student leader;


    public Klass() {
    }

    public Klass(int number) {
        this.number = number;
    }

    public int getNumber() {
        return number;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Klass klass = (Klass) o;
        return number == klass.number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number);
    }

    public void assignLeader(Student student) {
        if (!student.isIn(this)) {
            System.out.println("It is not one of us.");
        }
        this.leader = student;

        for (Object obs : observers) {
            ((MyObserver)obs).response(number,student.getName());
        }
    }

    public Boolean isLeader(Student student) {
        return leader == null? false : student.equals(leader);
    }

}
