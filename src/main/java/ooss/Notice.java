package ooss;

import java.util.ArrayList;

public abstract class Notice {

    protected ArrayList<MyObserver> observers = new ArrayList<>();

    //注册方法
    public void attach(MyObserver observer) {
        observers.add(observer);
    }

    //注销方法
    public void detach(MyObserver observer) {
        observers.remove(observer);
    }

    //通知函数
    public abstract void assignLeader(Student student);

}
