package ooss;

import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Teacher extends Person implements MyObserver {

    private ArrayList<Klass> klassList;

    public Teacher(int id, String name, int age) {
        super(id, name, age);
        klassList = new ArrayList<>();
    }

    @Override
    public String introduce() {
        StringBuilder sb = new StringBuilder();
        sb.append("My name is "+ getName() +". I am "+getAge()+" years old. I am a teacher.");
        if (!klassList.isEmpty()) {
            sb.append(" I teach Class ");
            klassList.stream().forEach(klass -> sb.append(klass.getNumber() + ", "));
            sb.delete(sb.length()-2,sb.length());
            sb.append(".");
        }
        return sb.toString();
    }

    public void assignTo(Klass klass) {
        this.klassList.add(klass);
    }

    public Boolean belongsTo(Klass klass) {
        for (Klass klass1 : klassList) {
            if (klass1.equals(klass)) {
                return true;
            }
        }
        return false;
    }

    public Boolean isTeaching(Student student) {
        Klass klass = student.getKlass();
        return klassList.contains(klass);
    }

    @Override
    public void response(int number,String leaderName) {
        System.out.println("I am "+getName()+", teacher of Class "+number+". I know "+leaderName+" become Leader.");
    }
}
