Objective: 1.I learned what Code Review is. At the same time, we reviewed the code with each other and learned other people’s Programming style.

2.I learned what Stream API and OOP are. We did relevant exercises.

Reflective:  Fulfilled.

Interpretive: Code review can help us correct our bad coding style. Stream API reduces the number of lines of code.

Decisional: We use code review to improve my programming style. And we can use Stream API appropriately to streamline code.